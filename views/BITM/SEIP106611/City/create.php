<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>City Selection</title>
	<link rel="stylesheet" href="../../../../Resource/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="../../../../Resource/css/style.css">
  </head>
  <body>
      <div class="create_wrapper">
	  		<div><h1 align="center">Select Your City</h1></div>
	  <br />
	  <br />
	  <br />
          <form class="form-horizontal" role="form" action="store.php" method="post">
                <div class="form-group">
                  <label class="control-label col-sm-3" for="field1">Name:</label>
                  <div class="col-sm-3">
                    <input type="text" name="name" class="form-control" id="field1" placeholder="write here" required="required" />
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-sm-3" for="field2">City:</label>
                  <div class="col-sm-9">
                    <div class="text">
                      <label>
						<select name="city">
							  <option value="Select your city">Select your city</option>
							  <option value="Dhaka">Dhaka</option>
							  <option value="Ranpur">Rangpur</option>
							  <option value="Chittagong">Chittagong</option>
							  <option value="shylet">sylhet</option>
							  <option value="Cumilla">Comilla</option>
							  <option value="Rajshahi">Rajshahi</option>
							  <option value="maymonshing">Mymensingh</option>
							  <option value="Nohakhali">Noakhali</option>
						</select>
					  </label>

					</div>
                  </div>
                </div>
                <div class="form-group">        
                  <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-default" name="submit">Submit</button>
                  </div>
                </div>
              </form>
          <p class="text-center"><a href="index.php">Back</a></p>
      </div>
      
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../../../../Resource/bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>